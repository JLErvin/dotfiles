<div align='center'>
    <h1>dotfiles</h1><br>
</div>

![Screenshot](https://i.imgur.com/cp7nj2V.png)

## System Configuration

* `distro`: void linux
* `wm`: berry
* `terminal`: rxvt-unicode
* `browser`: firefox
* `shell`: zsh
* `editor`: neovim
* `font`: dina
* `icon-font`: siji
* `bar`: lemonbar
* `launcher`: dmenu

For a better understanding of my dotfiles, I have included a README in each directory explaining my config
